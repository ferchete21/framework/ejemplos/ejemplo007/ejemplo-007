<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionEjercicio1(){
        $model = new \app\models\Ejercicio1();

    if ($model->load(Yii::$app->request->post())) {
        if ($model->validate()) {
            //Introducimos el bucle FOR
            $salida='';
            for($c=1;$c<=$model->numero1;$c++){
                $salida=$salida.'<br>'.$c;
            }
            return $this->render('resultadoEjercicio1',[
              'salida'=>$salida,  
            ]);
        } 
    }

    return $this->render('ejercicio1', [
        'model' => $model,
    ]);
    }
    
    public function actionEjercicio2(){
            $model = new \app\models\Ejercicio2();

    if ($model->load(Yii::$app->request->post())) {
        if ($model->validate()) {
             $salida=''; //Inicializamos la variable
            for($c=$model->numero1;$c>=1;$c--){//Creamos el bucle FOR descendente
              $salida=$salida.'<br>'.$c;
            }
            return $this->render('resultadoEjercicio2',[
              'salida'=>$salida,  
            ]);
        }
    }

    return $this->render('ejercicio2', [
        'model' => $model,
    ]);
    }
    
    public function actionEjercicio3(){
        return $this->render('ejercicio3');
    }
    
    public function actionEjercicio4(){
        return $this->render('ejercicio4');
    }
}
