<?php

namespace app\models;

use yii\base\Model;


class Ejercicio2 extends Model{
   public $numero1;
   
   
   public function attributeLabels() {
       return [
         "numero1" => 'Escribe un numero mayor que 10',
       ];
   }
   
    public function rules(){
        return [
            ['numero1','integer','min'=>10, 'max'=>100], //Con el integer le decimos que va a ser un numero
            ['numero1','required'] //para que sea obligatorio meter un dato
        ];
    }
}
