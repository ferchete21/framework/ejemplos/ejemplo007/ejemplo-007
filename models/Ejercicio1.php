<?php
namespace app\models;

use yii\base\Model;

class Ejercicio1 extends Model {
    public $numero1;
    
    
    
    
    public function attributeLabels() {
        return[
          "numero1" => "Escribe un numero del 1 al 100",
          
            ];
    }
    
    public function rules(){
        return [
            ['numero1','integer','min'=>1, 'max'=>100], //Con el integer le decimos que va a ser un numero
            ['numero1','required'] //para que sea obligatorio meter un dato
        ];
    }
    
}